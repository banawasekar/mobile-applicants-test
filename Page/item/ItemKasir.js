import React, { Component } from 'react';
import {
    Image,
    Text,
    View,
    Dimensions,
    TouchableOpacity
} from 'react-native';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

console.disableYellowBox = true;

export default class ItemKasir extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                id: this.props.id,
                menu: this.props.menu,
                harga: this.props.harga,
                gambar: this.props.gambar,
            }
        }
    }

    onPress = () => {
        this.props.onPressItem(this.state.data);
    };

    render() {
        return (
            <TouchableOpacity
                style={{
                    flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e9e9e9'
                }} key={this.props.id} onPress={this.onPress}>
                <View style={{ backgroundColor: 'white', margin: 10, alignItems: 'center' }}>
                    <View style={{ height: height * 15 / 100, width: width * 30 / 100 }}>
                        <Image
                            source={this.props.gambar}
                            style={{ height: '100%', width: '100%', resizeMode: 'contain' }}
                        />
                    </View>
                    <Text style={{ color: 'black', fontSize: 16, fontWeight: 'bold', marginTop: 10, marginBottom: 10 }}>{this.props.menu}</Text>
                </View>
            </TouchableOpacity >

        )
    }
}
