import React, { Component } from 'react';
import { View, Image, Dimensions, Text, TouchableOpacity, StyleSheet, FlatList, AsyncStorage, ImageBackground } from 'react-native';
import ItemKasir from './item/ItemKasir';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const Data = [
    {
        id: 1,
        menu: 'Nasi Pecel',
        harga: 25000,
        gambar: require('./assets/makanan/pecel.jpg'),

    },
    {
        id: 2,
        menu: 'Nasi Goreng',
        harga: 25000,
        gambar: require('./assets/makanan/nasi-goreng.jpg'),
    },
    {
        id: 3,
        menu: 'Nasi Kuninng',
        harga: 25000,
        gambar: require('./assets/makanan/nasi-kuning.jpg'),

    },
    {
        id: 4,
        menu: 'Ayam Geprek',
        harga: 25000,
        gambar: require('./assets/makanan/ayam-geprek.jpg'),
    },
    {
        id: 5,
        menu: 'Sate',
        harga: 25000,
        gambar: require('./assets/makanan/sate.jpg'),
    },
    {
        id: 6,
        menu: 'Rendang',
        harga: 25000,
        gambar: require('./assets/makanan/rendang.jpg'),
    },
    {
        id: 7,
        menu: 'gudeg',
        harga: 25000,
        gambar: require('./assets/makanan/gudeg.jpg'),
    },
    {
        id: 9,
        menu: 'nasi liwet',
        harga: 25000,
        gambar: require('./assets/makanan/nasi-liwet.jpg'),
    },
    {
        id: 10,
        menu: 'nasi uduk',
        harga: 25000,
        gambar: require('./assets/makanan/nasi-uduk.jpg'),

    },
]
var simpleArrayAll = [];
var simpleArray = [];
var total = null;
var data = [];
export default class HalamanKasir extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: (new Map() : Map<string, boolean>),
            count: 0,
            totalHarga: 0
        }

    }

    onPressItem1 = (item: object) => {

        this.setState((state) => {
            const selected = new Map(state.selected);
            selected.set(item, !selected.get(item));
            return { selected };
        });

        var newArray = simpleArray.slice();
        newArray.push(item.harga);

        for (i = 0; i < newArray.length; i++) {
            total += newArray[i];
        }

        simpleArrayAll.push(
            {
                id: item.id,
                menu: item.menu,
                harga: item.harga,
            }
        );

        this.setState({
            id: item.id,
            menu: item.menu,
            harga: item.harga,
            gambar: item.gambar,
            count: this.state.count + 1,
            totalHarga: total,
        });
    };


    keyExtractor1 = (item, index) => item.id;

    renderItemKasir = ({ item }) => {
        return (<ItemKasir
            id={item.id}
            menu={item.menu}
            harga={item.harga}
            gambar={item.gambar}
            onPressItem={this.onPressItem1}
            selected={!!this
                .state
                .selected
                .get(item.id)
            }
        />);
    };

    static navigationOptions = {
        title: 'KLOPOS',
        headerStyle: {
            backgroundColor: '#42b9f5',
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    }


    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>

                <View style={styles.container2}>
                    <View style={styles.container3}>
                        <FlatList
                            numColumns={3}
                            style={{ width: '100%' }}
                            data={Data}
                            keyExtractor={this.keyExtractor1}
                            extraData={this.state.selected}
                            renderItem={this.renderItemKasir}
                        />

                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={styles.containerFooter}>

                        <View style={{ alignItems: 'flex-start' }}>
                            <View style={styles.containerFooter2}>
                                <ImageBackground source={require('./assets/shopping-cart.png')} style={{ width: 20, height: 20, paddingBottom: 20 }} >
                                    <View style={styles.CircleShapeView}>
                                        <Text style={{ color: 'black', alignSelf: 'center', justifyContent: 'center' }}>{this.state.count}</Text>
                                    </View>
                                </ImageBackground>

                                <Text style={styles.textTotal}>Total</Text>

                            </View>
                        </View>


                        <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={() => navigate('Checkout', { harga: this.state.totalHarga, dataArray: simpleArrayAll, count: this.state.count })}>
                            <View style={styles.containerHarga}>
                                <Text style={styles.textHarga}>Rp. {this.state.totalHarga}</Text>
                                <Image source={require('./assets/right-arrow.png')} />
                            </View>
                        </TouchableOpacity>




                    </View>
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    container2: {
        flex: 1,
        backgroundColor: '#e9e9e9',
    },
    container3: {
        flex: 1,
        flexDirection: 'row',
        margin: 10,
    },
    cardList: {
        width: 100,
        height: 100,
        backgroundColor: 'blue',
        justifyContent: 'space-between'
    },
    containerFooter: {
        flex: 1,
        height: height * 9 / 100,
        backgroundColor: '#42b9f5',
        justifyContent: 'center',
        paddingLeft: 20,
        paddingTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width
    },
    containerFooter2: {
        alignItems: 'center',
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    textTotal: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        marginLeft: 40
    },
    textHarga: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        paddingLeft: 10
    },
    containerHarga: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    CircleShapeView: {
        width: 25,
        height: 25,
        borderRadius: 25 / 2,
        backgroundColor: 'yellow',
        marginLeft: 15,
    },

});