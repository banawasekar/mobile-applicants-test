import React, { Component } from 'react';
import { View, Image, Dimensions, Text, TouchableOpacity, StyleSheet, FlatList, ImageBackground } from 'react-native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const resultdata = [];
export default class HalamanCheckout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            menu: '',
            harga: '',
            jmlmakanan: '',
            jmlharga: ''
        }

    }


    static navigationOptions = {
        title: 'KLOPOS',
        headerStyle: {
            backgroundColor: '#42b9f5',
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    }

    componentDidMount() {
        dataPesanan = this.props.navigation.state.params.dataArray;
        const map = new Map();
        for (const item of dataPesanan) {
            if (!map.has(item.id)) {
                map.set(item.id, true);
                resultdata.push({
                    id: item.id,
                    menu: item.menu,
                    harga: item.harga,

                });
            }
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'white' }}>
                <FlatList
                    data={resultdata}
                    renderItem={({ item }) =>
                        <View style={{
                            flex: 1, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: 'grey',
                            borderBottomWidth: 0.5, paddingTop: 20, paddingBottom: 20
                        }}>
                            <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold' }}>{item.id}</Text>
                            <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold' }} key={item.id}>{item.menu}</Text>
                            <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold' }} key={item.id}>{item.harga}</Text>
                        </View>

                    }
                />

                <View style={{ flex: 1, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', }}>

                    <Text style={{ color: 'black', fontSize: 30, fontWeight: 'bold' }}>{this.props.navigation.state.params.menu}</Text>


                </View>

                <View style={{ flexDirection: 'row' }}>

                    <View style={{ flex: 1, height: height * 9 / 100, backgroundColor: '#42b9f5', justifyContent: 'center', paddingLeft: 20, paddingTop: 10, flexDirection: 'row', justifyContent: 'space-between', width: width }}>

                        <View style={{ alignItems: 'flex-start' }}>
                            <View style={styles.containerFooter2}>
                                <ImageBackground source={require('./assets/shopping-cart.png')} style={{ width: 20, height: 20, paddingBottom: 20 }} >
                                    <View style={styles.CircleShapeView}>
                                        <Text style={{ color: 'black', alignSelf: 'center', justifyContent: 'center' }}>{this.props.navigation.state.params.count}</Text>
                                    </View>
                                </ImageBackground>

                                <Text style={styles.textTotal}>Total</Text>

                            </View>
                        </View>


                        <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={() => navigate('Pembayaran', { harga: this.props.navigation.state.params.harga })}>
                            <View style={{
                                alignItems: 'center', flexDirection: 'row'
                            }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20, paddingLeft: 10 }}>Rp. {this.props.navigation.state.params.harga}</Text>
                                <Image
                                    source={require('./assets/right-arrow.png')}
                                />

                            </View>
                        </TouchableOpacity>

                    </View>

                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    view1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    containerFooter2: {
        alignItems: 'center',
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    textTotal: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        marginLeft: 40
    },
    CircleShapeView: {
        width: 25,
        height: 25,
        borderRadius: 25 / 2,
        backgroundColor: 'yellow',
        marginLeft: 15,
    },

})