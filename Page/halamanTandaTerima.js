import React, { Component } from 'react';
import { View, Image, Dimensions, Text, TouchableOpacity, StyleSheet } from 'react-native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

export default class HalamanTandaTerima extends Component {


    static navigationOptions = {
        title: 'Tanda Terima',
        headerStyle: {
            backgroundColor: '#42b9f5',
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    }


    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'white' }}>
                <View style={{ alignItems: 'center' }}>
                    <Image
                        source={require('./assets/checked-seccess.png')}
                    />
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <View style={styles.container2}>
                        <Text style={{ color: 'grey', fontSize: 20 }}>Tagihan Total</Text>
                        <Text style={{ color: '#42b9f5', fontSize: 20 }}>Rp. {this.props.navigation.state.params.harga}</Text>

                    </View>

                    <View styles={{ marginTop: 30 }}>
                        <View style={[styles.container2, {
                            borderBottomColor: 'grey',
                            borderBottomWidth: 0.5,
                            paddingBottom: 20
                        }]}>
                            <Text style={{ color: 'grey', fontSize: 20 }}>Tunai</Text>
                            <Text style={{ color: '#42b9f5', fontSize: 20 }}>Rp. {this.props.navigation.state.params.tunai}</Text>
                        </View>


                    </View>

                </View>

                <View style={[styles.container2, {
                    marginTop: 30, borderBottomColor: 'grey',
                    borderBottomWidth: 0.5,
                    paddingBottom: 20,
                    borderTopColor: 'grey',
                    borderTopWidth: 0.5,
                    paddingTop: 20
                }]}>
                    <Text style={{ color: 'grey', fontSize: 20 }}>Kembalian</Text>
                    <Text style={{ color: '#42b9f5', fontSize: 20 }}>Rp. {this.props.navigation.state.params.kembalian}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>

                    <TouchableOpacity style={{ flex: 1, height: height * 9 / 100, backgroundColor: '#42b9f5', justifyContent: 'center', paddingLeft: 20, paddingTop: 10, flexDirection: 'row', justifyContent: 'space-between', width: width }}  onPress={() => navigate('Kasir',{rifreshing : true})}>

                        <View style={{ alignItems: 'flex-start' }}>
                            <View style={{
                                alignItems: 'center', flexDirection: 'row', alignItems: 'flex-start'
                            }}>
                                <Image
                                    source={require('./assets/shopping-cart.png')}
                                />
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20, paddingLeft: 10 }}>Selesai</Text>
                            </View>
                        </View>


                        <TouchableOpacity style={{ alignItems: 'flex-end' }}>
                            <View style={{
                                alignItems: 'center', flexDirection: 'row'
                            }}>

                                <Image
                                    source={require('./assets/right-arrow.png')}
                                />

                            </View>
                        </TouchableOpacity>

                    </TouchableOpacity>

                </View>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        marginBottom: 10,
    },
    containerbayar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: 'white'
    }
})