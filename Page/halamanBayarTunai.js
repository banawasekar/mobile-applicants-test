import React, { Component } from 'react';
import { Alert, StyleSheet, StatusBar, View, Dimensions, Text, TouchableOpacity } from 'react-native';
import CalculatorResponse from './component/calculatorresponse';
import CalculatorButtonsContainer from './component/calculatorbuttonscontainer';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
export default class App extends Component {
    constructor() {
        super();
        this.state = {
            result: 0
        };
    }

    refresh() {
        this.setState({ result: 0 });
    }
    handleButtonPress(button) {
        debugger
        let oldinput = this.state.result,
            lastLetter;
        if (oldinput == 0) {
            oldinput = '';
        }
        switch (button) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                this.setState({
                    result: oldinput + button
                });
                break;
            case "C":
                this.setState({
                    result: 0
                });
                break;
            case "backspace":
                last = oldinput.slice(0, -1)
                this.setState({
                    result: last
                });
                break;
        }
    }
    static navigationOptions = {
        title: 'Bayar Tunai',
        headerStyle: {
            backgroundColor: '#42b9f5',
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    }
    render() {
        const { navigate } = this.props.navigation;
        const { result } = this.state;
        return (
            <View style={styles.container}>
                <CalculatorResponse
                    result={result}
                    refresh={this
                        .refresh
                        .bind(this)} />
                <CalculatorButtonsContainer
                    handleButtonPress={this
                        .handleButtonPress
                        .bind(this)} />
                <StatusBar barStyle="light-content" />
                <View style={{ flexDirection: 'row' }}>

                    <View style={{ flex: 1, height: height * 9 / 100, backgroundColor: 'white', justifyContent: 'center', paddingLeft: 20, paddingTop: 10, flexDirection: 'row', justifyContent: 'space-between', width: width }}>

                        <TouchableOpacity style={{ alignItems: 'flex-start' }} onPress={() => navigate('Pembayaran')}>
                            <View style={{
                                alignItems: 'center', flexDirection: 'row',
                            }}>

                                <Text style={{ color: 'grey', fontWeight: 'bold', fontSize: 20 }}>Batal</Text>
                            </View>
                        </TouchableOpacity>


                        <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={() => navigate('Pembayaran', { result: this.state.result })}>
                            <View style={{
                                alignItems: 'center', flexDirection: 'row', marginRight: 20
                            }}>

                                <Text style={{ color: '#42b9f5', fontWeight: 'bold', fontSize: 20 }}>OK</Text>
                            </View>
                        </TouchableOpacity>

                    </View>

                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e9e9e9'
    }
});