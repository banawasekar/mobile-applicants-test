import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity, Image, View } from 'react-native';
export default class CalculatorButton extends Component {
    render() {
        const { operator, handleButtonPress } = this.props;
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={() => handleButtonPress(operator)}>
                {
                    operator == 'backspace' ?
                        <View style={{ height: 50, width: 50, alignItems: 'center',justifyContent:'center' }}>
                            <Image
                                source={require('../assets/backspace.png')}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        :
                        <Text style={styles.item}>
                            {operator}
                        </Text>
                }

            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        margin: 5
    },
    item: {
        color: 'black',
        fontSize: 26
    }
});