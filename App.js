import { createStackNavigator, createAppContainer } from 'react-navigation';
import HalamanTandaTerima from './Page/halamanTandaTerima'
import HalamanPembayaran from './Page/halamanPembayaran';
import HalamanBayarTunai from './Page/halamanBayarTunai';
import HalamanKasir from './Page/halamanKasir';
import HalamanCheckout from './Page/halamanCheckout';
const MainNavigator = createStackNavigator({

  Kasir: {
    screen: HalamanKasir
  },
  Checkout: {
    screen: HalamanCheckout
  },
  Pembayaran: {
    screen: HalamanPembayaran
  },

  BayarTunai: {
    screen: HalamanBayarTunai
  },
  TandaTerima: {
    screen: HalamanTandaTerima
  }
})

const App = createAppContainer(MainNavigator);

export default App;